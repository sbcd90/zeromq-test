zeromq-test
===========

A POC to test different zeromq design patterns.

- [basic](src/main/java/org/zeromq/test/basic)
- [poller](src/main/java/org/zeromq/test/poller)
- [reqrply](src/main/java/org/zeromq/test/reqrply)
- [multiple workers](src/main/java/org/zeromq/test/multiple/workers)

spring-integration-mqtt test
============================

- [Install mosquitto](https://mosquitto.org/download/)
- Run mosquitto broker. mosquitto.exe
- Run mosquitto publisher. mosquitto_pub.exe
- Run Spring boot application.