package org.zeromq.test.poller;

import org.zeromq.ZMQ;

public class SyncServer {

    private final ZMQ.Context context;
    private final ZMQ.Socket sync;

    public SyncServer(int syncPort) {
        String syncUrl = "tcp://*:" + syncPort;

        this.context = ZMQ.context(1);
        this.sync = this.context.socket(ZMQ.PUSH);
        this.sync.bind(syncUrl);
    }

    public void sendMessage() throws Exception {
        while (!Thread.currentThread().isInterrupted()) {
            sync.send("Continue", 0);
            Thread.sleep(10000);
        }
        sync.close();
    }

    public static void main(String[] args) throws Exception {
        SyncServer syncServer = new SyncServer(5556);
        syncServer.sendMessage();
    }
}