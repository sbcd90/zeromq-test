package org.zeromq.test.poller;

import org.zeromq.ZMQ;

public class ZMQClient {

    private final ZMQ.Context context;
    private final ZMQ.Socket sync;
    private final ZMQ.Socket subscriber;
    private final ZMQ.Poller poller;

    public ZMQClient(int syncPort, int subscriberPort) {
        this.context = ZMQ.context(1);
        this.sync = this.context.socket(ZMQ.PULL);

        this.sync.connect("tcp://localhost:" + syncPort);

        this.subscriber = this.context.socket(ZMQ.SUB);
        this.subscriber.subscribe("".getBytes());
        this.subscriber.connect("tcp://localhost:" + subscriberPort);

        this.poller = new ZMQ.Poller(2);
        this.poller.register(this.sync, ZMQ.Poller.POLLIN);
        this.poller.register(this.subscriber, ZMQ.Poller.POLLIN);
    }

    public void recvAndsendMessage() {
        while (!Thread.currentThread().isInterrupted()) {
            this.poller.poll();

            if (this.poller.pollin(0)) {
                byte[] syncMessage = this.sync.recv(0);
                System.out.println(new String(syncMessage));
            }
            if (this.poller.pollin(1)) {
                byte[] subsMessage = this.subscriber.recv(0);
                System.out.println(new String(subsMessage));
            }
        }
        this.sync.close();
        this.subscriber.close();
    }

    public static void main(String[] args) {
        ZMQClient client = new ZMQClient(5556, 5558);
        client.recvAndsendMessage();
    }
}