package org.zeromq.test.poller;

import org.zeromq.ZMQ;

public class PubServer {

    private final ZMQ.Context context;
    private final ZMQ.Socket publisher;

    public PubServer(int publisherPort) {
        this.context = ZMQ.context(1);
        this.publisher = context.socket(ZMQ.PUB);

        this.publisher.bind("tcp://*:" + publisherPort);
    }

    public void sendMessage() throws Exception {
        while (!Thread.currentThread().isInterrupted()) {
            publisher.send("Hello World".getBytes(), 0);
            Thread.sleep(10000);
        }
        publisher.close();
    }

    public static void main(String[] args) throws Exception {
        PubServer pubServer = new PubServer(5558);
        pubServer.sendMessage();
    }
}