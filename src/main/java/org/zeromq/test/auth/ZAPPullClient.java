package org.zeromq.test.auth;

import org.zeromq.ZMQ;

public class ZAPPullClient {

    private final ZMQ.Context context;
    private final ZMQ.Socket pullSocket;
    private final ZMQ.Poller pullPoller;

    public ZAPPullClient(int pullPort) {
        this.context = ZMQ.context(1);
        this.pullSocket = context.socket(ZMQ.PULL);

        this.pullSocket.setPlainUsername("username1".getBytes());
        this.pullSocket.setPlainPassword("password".getBytes());
        this.pullSocket.connect("tcp://localhost:" + pullPort);

        this.pullPoller = new ZMQ.Poller(1);
        this.pullPoller.register(this.pullSocket, ZMQ.Poller.POLLIN);
    }

    public void recvAndSendMessage() {
        while (!Thread.currentThread().isInterrupted()) {
            this.pullPoller.poll();

            if (this.pullPoller.pollin(0)) {
                byte[] message = this.pullSocket.recv(0);
                System.out.println(new String(message));
            }
        }
        this.pullSocket.close();
        this.context.close();
    }

    public static void main(String[] args) {
        ZAPPullClient authClient = new ZAPPullClient(5556);
        authClient.recvAndSendMessage();
    }
}