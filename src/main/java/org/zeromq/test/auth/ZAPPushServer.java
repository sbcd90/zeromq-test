package org.zeromq.test.auth;

import org.zeromq.ZAuth;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class ZAPPushServer {

    private final ZMQ.Context context;
    private final ZMQ.Socket pushSocket;
    private final ZMQ.Poller pushPoller;

    public ZAPPushServer(int pushPort) throws Exception {
        String prefix = "passwords";
        String suffix = "txt";

        File file = File.createTempFile(prefix, suffix);
        String str = "username=password";
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(str);
        writer.close();

        this.context = ZMQ.context(1);
        ZContext zCtx = new ZContext();
        this.pushSocket = zCtx.createSocket(ZMQ.PUSH);

        ZAuth auth = new ZAuth(zCtx);
        auth.setVerbose(true);
//        auth.allow("127.0.0.1");

        auth.configurePlain("*", file.getAbsolutePath());

        this.pushSocket.setZAPDomain("global".getBytes());
        this.pushSocket.setPlainServer(true);
        this.pushSocket.bind("tcp://*:" + pushPort);

        this.pushPoller = new ZMQ.Poller(1);
        this.pushPoller.register(this.pushSocket, ZMQ.Poller.POLLOUT);
    }

    public void sendMessage() {
        while (!Thread.currentThread().isInterrupted()) {
            this.pushPoller.poll();

            if (this.pushPoller.pollout(0)) {
                this.pushSocket.send("Hello World".getBytes(), 0);
            }
        }
        this.pushSocket.close();
        this.context.close();
    }

    public static void main(String[] args) throws Exception {
        ZAPPushServer authServer = new ZAPPushServer(5556);
        authServer.sendMessage();
    }
}