package org.zeromq.test.basic;

import org.zeromq.ZMQ;

public class Subscriber2 {
    private final ZMQ.Context context;
    private final ZMQ.Socket subscriber;
    private final ZMQ.Socket sync;
    private final String syncUrl;
    private final String subscriberUrl;

    public Subscriber2(int syncPort, int subscriberPort, int ioThreads) {
        this.context = ZMQ.context(ioThreads);

        this.subscriber = this.context.socket(ZMQ.SUB);
        subscriber.setIdentity("Subscriber2".getBytes());

        this.sync = context.socket(ZMQ.PUSH);

        this.syncUrl = "tcp://localhost:" + syncPort;
        this.subscriberUrl = "tcp://localhost:" + subscriberPort;

        subscriber.subscribe("topic1".getBytes());
        subscriber.connect(subscriberUrl);
        sync.connect(syncUrl);
    }

    public void sendUpMessage() {
        sync.send("Hi!Subscriber2 is up!".getBytes(), 0);
        System.out.println("Subscriber2 is up");
    }

    private boolean pingPublisherForMessages() {
        return sync.send("Hi! I Subscriber2 want more messages", 0);
    }

    public void recvAndSendData() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            byte[] rawBytes = subscriber.recv(0);
            String data = new String(rawBytes);

            System.out.println(data);
            System.out.println(rawBytes.length);
        }

        long end = System.currentTimeMillis();
        System.out.println("time taken to receive messages : " + (end - start));
    }

    public static void main(String[] args) throws Exception {
        Subscriber2 subscriber = new Subscriber2(5561, 5562, 1);
        subscriber.sendUpMessage();
        subscriber.recvAndSendData();

        Thread.sleep(60000);
        subscriber.pingPublisherForMessages();
    }
}