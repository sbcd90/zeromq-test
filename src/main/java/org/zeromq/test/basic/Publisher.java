package org.zeromq.test.basic;

import org.zeromq.ZMQ;


public class Publisher {

    private final ZMQ.Context context;
    private final ZMQ.Socket publisher;
    private final ZMQ.Socket sync;
    private final String syncUrl;
    private final String publisherUrl;

    public Publisher(int syncPort, int publisherPort) {
        String syncUrl = "tcp://*:" + syncPort;
        this.syncUrl = syncUrl;

        String publisherUrl = "tcp://*:" + publisherPort;
        this.publisherUrl = publisherUrl;

        context = ZMQ.context(1);
        publisher = context.socket(ZMQ.PUB);
        sync = context.socket(ZMQ.PULL);

        sync.bind(syncUrl);
        publisher.bind(publisherUrl);
    }

    public static void main(String[] args) {
        Publisher publisher = new Publisher(5561, 5562);
        publisher.sendUpMessage();
        publisher.recvAndSendData();
    }

    public void sendUpMessage() {
        System.out.println("Publisher running");
    }

    private boolean checkIfSubscriberPings(byte[] message) {
        return new String(message).contains("Subscriber");
    }

    public void recvAndSendData() {
        byte[] message;
        while((message = sync.recv(0)) != null) {
            if (checkIfSubscriberPings(message)) {
                long start = System.currentTimeMillis();
                int i;

                for (i=0;i < 10;i++) {
                    publisher.send(("topic1 " + new String(message)).getBytes(), 0);
                }

                long end = System.currentTimeMillis();
                System.out.println("Time difference: " + (end - start));
            }
        }
    }
}