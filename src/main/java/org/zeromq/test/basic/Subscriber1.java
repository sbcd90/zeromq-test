package org.zeromq.test.basic;

import org.zeromq.ZMQ;

public class Subscriber1 {
    private final ZMQ.Context context;
    private final ZMQ.Socket subscriber;
    private final ZMQ.Socket sync;
    private final String syncUrl;
    private final String subscriberUrl;

    public Subscriber1(int syncPort, int subscriberPort) {
        this.context = ZMQ.context(1);

        this.subscriber = context.socket(ZMQ.SUB);
        subscriber.setIdentity("hello".getBytes());

        this.sync = context.socket(ZMQ.PUSH);

        this.syncUrl = "tcp://localhost:" + syncPort;
        this.subscriberUrl = "tcp://localhost:" + subscriberPort;

        subscriber.subscribe("topic1".getBytes());
        subscriber.connect(subscriberUrl);
        sync.connect(syncUrl);
    }

    public void sendUpMessage() {
        sync.send("Hi!Subscriber1 is up!".getBytes(), 0);
        System.out.println("Subscriber1 is up");
    }

    public void sendAndRecvData() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            byte[] rawBytes = subscriber.recv(0);

            System.out.println(rawBytes.length);
            System.out.println(new String(rawBytes));
        }
        long end = System.currentTimeMillis();
        long diff = (end - start);
        System.out.println("time taken to receive messages is :"
                + diff);
    }

    public static void main(String[] args) {
        Subscriber1 subscriber = new Subscriber1(5561, 5562);
        subscriber.sendUpMessage();
        subscriber.sendAndRecvData();
    }
}