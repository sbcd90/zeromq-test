package org.zeromq.test.spring;

import java.io.*;

public class TempFileTest {

    public static void main(String[] args) throws Exception {
        String prefix = "passwords";
        String suffix = "txt";

        File file = File.createTempFile(prefix, suffix);
        String str = "username:password";
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(str);
        writer.close();

        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        reader.close();
    }
}