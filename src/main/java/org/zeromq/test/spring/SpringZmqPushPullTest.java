package org.zeromq.test.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.endpoint.MessageProducerSupport;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.stream.CharacterStreamReadingMessageSource;
import org.springframework.integration.zmq.core.DefaultZmqClientFactory;
import org.springframework.integration.zmq.core.ZmqClientFactory;
import org.springframework.integration.zmq.inbound.ZmqMessageDrivenChannelAdapter;
import org.springframework.integration.zmq.outbound.ZmqMessageHandler;
import org.springframework.integration.zmq.support.DefaultZmqMessageConverter;
import org.springframework.messaging.MessageHandler;
import org.zeromq.ZMQ;

@SpringBootApplication
public class SpringZmqPushPullTest {

    private static final Logger LOG = LoggerFactory.getLogger(SpringZmqPushPullTest.class);

    @Bean
    public ZmqClientFactory zmqClientFactoryPush() {
        DefaultZmqClientFactory factory = new DefaultZmqClientFactory();
        factory.setServerURI("tcp://*:5556");
        factory.setClientType(ZMQ.PUSH);
        return factory;
    }

    @Bean
    public ZmqClientFactory zmqClientFactoryPull() {
        DefaultZmqClientFactory factory = new DefaultZmqClientFactory();
        factory.setServerURI("tcp://localhost:5556");
        factory.setClientType(ZMQ.PULL);
        return factory;
    }

    @Bean
    public IntegrationFlow zmqOutFlow() {
        return IntegrationFlows.from(CharacterStreamReadingMessageSource.stdin(),
                                    e -> e.poller(Pollers.fixedDelay(1000)))
                                .transform(p -> p + " sent to ZMQ")
                                .handle(zmqOutbound())
                                .get();
    }

    @Bean
    public MessageHandler zmqOutbound() {
        ZmqMessageHandler messageHandler = new ZmqMessageHandler("simplePublisher", zmqClientFactoryPush());
        return messageHandler;
    }

    @Bean
    public IntegrationFlow zmqInFlow() {
        return IntegrationFlows.from(zmqInbound())
                             .transform(p -> p + ", received from ZMQ")
                             .handle(logger())
                             .get();
    }

    private LoggingHandler logger() {
        LoggingHandler loggingHandler = new LoggingHandler("INFO");
        loggingHandler.setLoggerName("sample");
        return loggingHandler;
    }

    @Bean
    public MessageProducerSupport zmqInbound() {
        ZmqMessageDrivenChannelAdapter adapter = new ZmqMessageDrivenChannelAdapter("simpleConsumer", zmqClientFactoryPull());
        adapter.setConverter(new DefaultZmqMessageConverter());
        return adapter;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringZmqPushPullTest.class, args);
    }
}