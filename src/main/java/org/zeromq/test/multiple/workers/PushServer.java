package org.zeromq.test.multiple.workers;

import org.zeromq.ZMQ;

public class PushServer {

    private final ZMQ.Context context;
    private final ZMQ.Socket pushSocket;
    private final ZMQ.Poller poller;

    public PushServer(int pushPort) {
        this.context = ZMQ.context(1);
        this.pushSocket = context.socket(ZMQ.PUSH);

        this.pushSocket.bind("tcp://*:" + pushPort);

        this.poller = new ZMQ.Poller(1);
        this.poller.register(this.pushSocket, ZMQ.Poller.POLLOUT);
    }

    public void sendMessage() {
        boolean flag = false;
        while (!Thread.currentThread().isInterrupted()) {
            this.poller.poll();

            if (this.poller.pollout(0) && !flag) {
                pushSocket.send("Hello Worker! I'm server".getBytes(), 0);
                flag = true;
            }
        }
        pushSocket.close();
        context.term();
    }

    public static void main(String[] args) {
        PushServer server = new PushServer(5556);
        server.sendMessage();
    }
}