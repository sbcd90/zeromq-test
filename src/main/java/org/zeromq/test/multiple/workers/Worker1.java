package org.zeromq.test.multiple.workers;

import org.zeromq.ZMQ;

public class Worker1 {

    private final ZMQ.Context context;
    private final ZMQ.Socket pullSocket;
    private final ZMQ.Socket pushSocket;
    private final ZMQ.Poller poller;

    public Worker1(int pullPort, int pushPort) {
        this.context = ZMQ.context(1);
        this.pullSocket = context.socket(ZMQ.PULL);
        this.pushSocket = context.socket(ZMQ.PUSH);

        this.pushSocket.bind("tcp://*:" + pushPort);

        this.pullSocket.connect("tcp://localhost:" + pullPort);

        this.poller = new ZMQ.Poller(2);
        this.poller.register(this.pullSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.pushSocket, ZMQ.Poller.POLLOUT);
    }

    public void recvAndSendMessage() throws Exception {

        while (!Thread.currentThread().isInterrupted()) {
            this.poller.poll();

            if (this.poller.pollin(0)) {
               byte[] message = pullSocket.recv(0);
               Thread.sleep(10000);
               System.out.println(new String(message));
            }
            if (this.poller.pollout(1)) {
                pushSocket.send("Hello Sink! I'm Worker1!".getBytes(), 0);
            }
        }
        pushSocket.close();
        pullSocket.close();
        context.term();
    }

    public static void main(String[] args) throws Exception {
        Worker1 worker1 = new Worker1(5556, 5557);
        worker1.recvAndSendMessage();
    }


}