package org.zeromq.test.multiple.workers;

import org.zeromq.ZMQ;

public class Sink {

    private final ZMQ.Context context;
    private final ZMQ.Socket pullSocket1;
    private final ZMQ.Socket pullSocket2;
    private final ZMQ.Poller poller;

    public Sink(int pullPort1, int pullPort2) {
        this.context = ZMQ.context(1);
        this.pullSocket1 = context.socket(ZMQ.PULL);
        this.pullSocket2 = context.socket(ZMQ.PULL);

        this.pullSocket1.connect("tcp://localhost:" + pullPort1);
        this.pullSocket2.connect("tcp://localhost:" + pullPort2);

        this.poller = new ZMQ.Poller(2);
        this.poller.register(this.pullSocket1, ZMQ.Poller.POLLIN);
        this.poller.register(this.pullSocket2, ZMQ.Poller.POLLIN);
    }

    public void recvMessage() {
        while (!Thread.currentThread().isInterrupted()) {
            this.poller.poll();

            if (this.poller.pollin(0)) {
                byte[] message = pullSocket1.recv(0);
                System.out.println(new String(message));
            }
            if (this.poller.pollin(1)) {
                byte[] message = pullSocket2.recv(0);
                System.out.println(new String(message));
            }
        }
        pullSocket1.close();
        pullSocket2.close();
        context.term();
    }

    public static void main(String[] args) {
        Sink sink = new Sink(5557, 5558);
        sink.recvMessage();
    }
}