package org.zeromq.test.multiple.workers;

import org.zeromq.ZMQ;

public class Worker2 {

    private final ZMQ.Context context;
    private final ZMQ.Socket pushSocket;
    private final ZMQ.Socket pullSocket;
    private final ZMQ.Poller poller;

    public Worker2(int pullPort, int pushPort) {
        this.context = ZMQ.context(1);
        this.pullSocket = context.socket(ZMQ.PULL);
        this.pushSocket = context.socket(ZMQ.PUSH);

        this.pullSocket.connect("tcp://localhost:" + pullPort);
        this.pushSocket.bind("tcp://*:" + pushPort);

        this.poller = new ZMQ.Poller(2);
        this.poller.register(pullSocket, ZMQ.Poller.POLLIN);
        this.poller.register(pushSocket, ZMQ.Poller.POLLOUT);
    }

    public void recvAndSendMessage() throws Exception {

        while (!Thread.currentThread().isInterrupted()) {
            this.poller.poll();

            if (this.poller.pollin(0)) {
                byte[] message = pullSocket.recv(0);
                Thread.sleep(10000);
                System.out.println(new String(message));
            }
            if (this.poller.pollout(1)) {
                pushSocket.send("Hello Sink! I'm Worker2".getBytes(), 0);
            }
        }
        pushSocket.close();
        pullSocket.close();
        context.term();
    }

    public static void main(String[] args) throws Exception {
        Worker2 worker2 = new Worker2(5556, 5558);
        worker2.recvAndSendMessage();
    }
}