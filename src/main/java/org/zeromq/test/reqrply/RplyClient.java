package org.zeromq.test.reqrply;

import org.zeromq.ZMQ;

public class RplyClient {

    private final ZMQ.Context context;
    private final ZMQ.Socket repSocket;
    private final ZMQ.Poller poller;

    public RplyClient(int rplyPort) {
        this.context = ZMQ.context(1);
        this.repSocket = context.socket(ZMQ.REQ);

        this.repSocket.bind("tcp://*:" + rplyPort);

        this.poller = new ZMQ.Poller(1);
        this.poller.register(this.repSocket, ZMQ.Poller.POLLIN | ZMQ.Poller.POLLOUT);
    }

    public void recvAndSendMessage() throws Exception {
        while (!Thread.currentThread().isInterrupted()) {
            this.poller.poll();

            if (this.poller.pollout(0)) {
                repSocket.send("Hi from Client through pollout".getBytes(), 0);
            }
            if (this.poller.pollin(0)) {
                byte[] message = repSocket.recv(0);
                System.out.println(new String(message));
            }
        }
        repSocket.close();
        context.term();
    }

    public static void main(String[] args) throws Exception {
        RplyClient client = new RplyClient(5556);
        client.recvAndSendMessage();
    }


}