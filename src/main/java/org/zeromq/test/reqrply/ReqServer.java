package org.zeromq.test.reqrply;

import org.zeromq.ZMQ;

public class ReqServer {

    private final ZMQ.Context context;
    private final ZMQ.Socket reqSocket;
    private final ZMQ.Poller poller;

    public ReqServer(int reqPort) {
        this.context = ZMQ.context(1);
        this.reqSocket = context.socket(ZMQ.REP);

        this.reqSocket.connect("tcp://localhost:" + reqPort);

        this.poller = new ZMQ.Poller(1);
        this.poller.register(this.reqSocket, ZMQ.Poller.POLLOUT | ZMQ.Poller.POLLIN);
    }

    public void recvAndSendMessage() throws Exception {
        while (!Thread.currentThread().isInterrupted()) {
            poller.poll();

            if (poller.pollin(0)) {
                byte[] receivedMsg = reqSocket.recv(0);
                System.out.println(new String(receivedMsg));
//                Thread.sleep(10000);
//                reqSocket.send("Hi from Server through pollin".getBytes(), 0);
            }
            if (poller.pollout(0)) {
                reqSocket.send("Hi from Server through pollout".getBytes(), 0);
            }
        }
        reqSocket.close();
        context.term();
    }

    public static void main(String[] args) throws Exception {
        ReqServer server = new ReqServer(5556);
        server.recvAndSendMessage();
    }
}